import sympy
from sympy import Array 
from sympy import *
import numpy as np
import sys

#Piez(3,4,5,6) a faire  done
#chercher harmonique ordre 3 
#fonction Tenseur harmonique a1--a5 qui prend uniquement une lettre en entrée eet sort matice a
#configure gitlab    

#code en entre T i j


R=3 
    
class Tensor :
      
    def Tens(self,u,n,d):     

        def Mat(m,n):  
            
            c=f=[]
            for i in range(m):
                for j in range(n):
                    a=u+'{}{}'.format(i+1,j+1)
                    c.append(sympy.core.symbol.Symbol(a))
                    P=MutableSparseNDimArray(c,(m,n))
            return P
        
        def rec1(n,d):      
            
            c=[]
            f=[] 
  
            if n==1:
                
                a=[]         
                for i in range(n):
                    a.append(d)
                    
                for i in range(d):
                    
                    a=u+'{}'.format(i+1)
                    c.append(sympy.core.symbol.Symbol(a))
                P=MutableSparseNDimArray(c,(d,1))
                return c
                          
            else:

                for i in range(d**(n-1)):
                    for j in range(d):
                        
                        x=str(rec1(n-1,d)[i])+'{}'.format(j+1)   
                        f.append(sympy.core.symbol.Symbol(x))           
                return f
               
        a=[]
        for i in range(n):
            a.append(d)   
        if n==1:
            P=MutableSparseNDimArray(rec1(1,d),[d,1])
        
        elif type(d)==list:
            
                t=d
                P=Mat(t[0],t[1])
        
        else :
            P=MutableSparseNDimArray(rec1(n,d),a)
            
        return P
    
    # def Input_Tensor(self):    #Tenseur ordre n et dim d
        
    #     r=self.rec1(self.n,self.d)
    #     a=[]
        
    #     for i in range(self.n):
    #         a.append(self.d)
        
    #     P=MutableSparseNDimArray(r,a)
    #     # print(P)
    #     return P
    
    
    def Rot_Tens(self, T, θ):
        
        dim = T.shape[0]
        
        if T.shape[0]==3 :
            if type(θ)==list :
            
                cx = [1,0,0,0,cos(θ[0]),-sin(θ[0]),0,sin(θ[0]),cos(θ[0])]
                cy = [cos(θ[1]),0,sin(θ[1]),0,1,0,-sin(θ[1]),0,cos(θ[1])]
                cz = [cos(θ[2]),-sin(θ[2]),0,sin(θ[2]),cos(θ[2]),0,0,0,1]
                Rx = MutableSparseNDimArray(cx,(3,3))
                Ry = MutableSparseNDimArray(cy,(3,3))
                Rz = MutableSparseNDimArray(cz,(3,3))
                
                
                if len(θ)== 3 and θ[0]!=0 and θ[1]!=0 and θ[2]!=0 :
                    R = Matrix(Rz)*Matrix(Ry)*Matrix(Rx)
                elif len(θ)==3 and θ[2]==0 :
                    R = Matrix(Rx)*Matrix(Ry)
                    
                elif len(θ)==3 and θ[1]==0 :
                    R = Matrix(Rx)*Matrix(Rz)
                    
                elif len(θ)==3 and θ[0]==0 :
                    R = Matrix(Ry)*Matrix(Rz)
                    
                elif len(θ)==3 and θ[0]==θ[1]==0 :
                    R = Matrix(Rz)
                    
                elif len(θ)==3 and θ[1]==θ[2]==0 :
                    R = Matrix(Rx)
                    
                elif len(θ)==3 and θ[0]==θ[2]==0 :
                    R = Matrix(Ry)
        
            else  :
                
                cz = [cos(θ),-sin(θ),0,sin(θ),cos(θ),0,0,0,1]
                Rz = MutableSparseNDimArray(cz,(3,3))
                R = Matrix(Rz)
            
               
            a=[]
            for i in range(T.rank()):
                a.append(dim)
            
            P2 = MutableDenseNDimArray.zeros(*a)                
            
            
            if T.shape[1]==1 :
                P2 = MutableDenseNDimArray.zeros(dim,1)          
                for i in range(dim):
                    s=0
                    for j in range(dim):
                        s = s + R[i,j]*T[j,0]
                    P2[i,0]=s
             
                    
            elif T.rank()==2 :        
             
                for i in range(dim):
                    for j in range(dim):
                        
                            
                        s=0
                        for p in range(dim):
                                
                            for q in range(dim):
                                    
                                
                                s = s + R[i,p]*R[j,q]*T[p,q]
                                                       
                        P2[i,j]=s       
             
            elif T.rank()==3 :     
            
                for i in range(dim):
                    for j in range(dim):
                        for k in range(dim):
                            
                            s=0
                            for p in range(dim):
                                
                                for q in range(dim):
                                    
                                    for r in range(dim):
                                        s = s + R[i,p]*R[j,q]*R[k,r]*T[p,q,r]
                                                       
                            P2[i,j,k]=s
                        
            elif T.rank()==4 :     
            
                for i in range(dim):
                    for j in range(dim):
                        for k in range(dim):
                            for l in range(dim):
                            
                                s=0
                                for p in range(dim):
                                    for q in range(dim):
                                        for r in range(dim):
                                            for t in range(dim):
                                                s = s + R[i,p]*R[j,q]*R[k,r]*R[l,t]*T[p,q,r,t]
                                                           
                                P2[i,j,k,l]=s
            
            else :
                
                print("\nInvalid input, this function doesnt work for tensor ranks superior to 4 or rank 0 tensors")
                sys.exit()
            
            return (simplify(P2),simplify(R))
        
        
        elif T.shape[0]==2 :
            if type(θ)==list and (len(θ)!=2 and len(θ)!=3) or type(θ)!=list  :
                cz = [cos(θ),sin(θ),-sin(θ),cos(θ)]
                Rz = MutableSparseNDimArray(cz,(2,2)) 
                R = Matrix(Rz)
            
            a=[]
            for i in range(T.rank()):
                a.append(dim)
            
            P2 = MutableDenseNDimArray.zeros(*a)                
            
            
            if T.shape[1]==1 :
                P2 = MutableDenseNDimArray.zeros(dim,1)          
                for i in range(dim):
                    s=0
                    for j in range(dim):
                        s = s + R[i,j]*T[j,0]
                    P2[i,0]=s
             
                    
            elif T.rank()==2 :        
             
                for i in range(dim):
                    for j in range(dim):
                        
                            
                        s=0
                        for p in range(dim):
                                
                            for q in range(dim):
                                    
                                
                                s = s + R[i,p]*R[j,q]*T[p,q]
                                                       
                        P2[i,j]=s       
             
            elif T.rank()==3 :     
            
                for i in range(dim):
                    for j in range(dim):
                        for k in range(dim):
                            
                            s=0
                            for p in range(dim):
                                
                                for q in range(dim):
                                    
                                    for r in range(dim):
                                        s = s + R[i,p]*R[j,q]*R[k,r]*T[p,q,r]
                                                       
                            P2[i,j,k]=s
                        
            elif T.rank()==4 :     
            
                for i in range(dim):
                    for j in range(dim):
                        for k in range(dim):
                            for l in range(dim):
                            
                                s=0
                                for p in range(dim):
                                    for q in range(dim):
                                        for r in range(dim):
                                            for t in range(dim):
                                                s = s + R[i,p]*R[j,q]*R[k,r]*R[l,t]*T[p,q,r,t]
                                                           
                                P2[i,j,k,l]=s
            
            else :
                
                print("\nInvalid input, this function doesnt work for tensor ranks superior to 4 or rank 0 tensors")
                sys.exit()
            
            return (simplify(P2),simplify(R))

        else :   

                print("\nInvalid input, this function doesnt work for tensor dimensions different than 2 or 3")
                sys.exit()             



    def Tens3_Sym(self,s):   #Tenseur ordre 3 totalement symetrique
        
        R=3
        
        P=self.Tens(s,3,3)
        for i in range (R):
            for j in range (R):
                for k in range (R):
        
                    P[i,j,k]=P[i,k,j]=P[k,j,i]=P[j,i,k]
        return P
    
    def Mat(self,u,m,n):
        
        z=[]
        c=[]
        f=[]
        s=0
        k=0
        
        for i in range(m):
            for j in range(n):
                
                
                a=u+'{}{}'.format(i+1,j+1)
                # print('\na=',a)
                c.append(sympy.core.symbol.Symbol(a))
                P=MutableSparseNDimArray(c,(m,n))
        return P    
    
    
    def Piezo_Symb(self,u):   # Tenseur piezo symbolique
        
        P=self.Tens(u,3,3)
        
        for i in range (R):
            for j in range (R):
                for k in range (R):
        
                    P[i,j,k]=P[i,k,j]
        
        # print('\n[P] =', P)
        return P

    def Piezo_Num(self,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r):   # Tenseur Piezo numérique. Entrer 18/27 composantes
        
    
        #composantes triangulaires superieurs
        

        t=[a,b,c,b,d,e,c,e,f,g,h,i,h,j,k,i,k,l,m,n,o,n,p,q,o,q,r]
        
        
        P=MutableSparseNDimArray(t,(3,3,3))
            
        
        for i in range (R):
            for j in range (R):
                for k in range (R):
            
                    P[i,j,k]=P[i,k,j]
            
        return P
    
    
    

    
    def Tens3_To_Kelv(self, P):    #Tenseur ordre 3 quelconques to matrice de Kelvin
            
            
        c=([P[0,0,0],P[0,0,1],P[0,2,2],sqrt(2)*P[0,1,2],sqrt(2)*P[0,0,2],sqrt(2)*P[0,0,1],[P[1,0,0],P[1,1,1],P[1,2,2],sqrt(2)*P[1,1,2],sqrt(2)*P[1,0,2],sqrt(2)*P[1,0,1]],[P[2,0,0],P[2,1,1],P[2,2,2],sqrt(2)*P[2,1,2],sqrt(2)*P[2,0,2],sqrt(2)*P[2,0,1]]])
        p = MutableSparseNDimArray(c,(3,6)) 
            
        # print('\n[p] =', p)
            
        return p    
        
        
    def Tens3_To_Voigt(self, P):    #Tenseur piezo to matrice de Voigt
        
       
        
        d = ([P[0,0,0],P[0,0,1],P[0,2,2],P[0,1,2],P[0,0,2],P[0,0,1],[P[1,0,0],P[1,1,1],P[1,2,2],P[1,1,2],P[1,0,2],P[1,0,1]],[P[2,0,0],P[2,1,1],P[2,2,2],P[2,1,2],P[2,0,2],P[2,0,1]]])
        pv = MutableSparseNDimArray(d,(3,6)) 
        # print('\n[pv] =', pv)
        return pv
    
    
    
    

    

class Harm(Tensor) :    #Class des harmoniques intrinsèques



    def Harm_a(self,u):
        
        a = MutableSparseNDimArray(range(9),(R,R))
        a[1,2]=a[2,1]=sympy.core.symbol.Symbol(u+"{}{}".format(2,3))
        a[0,2]=a[2,0]=sympy.core.symbol.Symbol(u+"{}{}".format(1,3))
        a[0,1]=a[1,0]=sympy.core.symbol.Symbol(u+"{}{}".format(1,2))
        
        for i in range(R):
            a[i,i]=sympy.core.symbol.Symbol(u+"{}{}".format(i+1,i+1))
            
        a[2,2]=-a[0,0]-a[1,1]
                            
        return a
    
    def Harm_H(self,u):
        
        H = MutableSparseNDimArray(range(50),(R,R,R))
        
        H[1,1,0]=H[1,0,1]=H[0,1,1]=sympy.core.symbol.Symbol(u+"{}".format(4))
        H[1,0,0]=H[0,1,0]=H[0,0,1]=sympy.core.symbol.Symbol(u+"{}".format(5))
        H[2,0,0]=H[0,2,0]=H[0,0,2]=sympy.core.symbol.Symbol(u+"{}".format(6))
        H[0,1,2]=H[0,2,1]=H[2,0,1]=H[2,1,0]=H[1,2,0]=H[1,0,2]=sympy.core.symbol.Symbol(u+"{}".format(7))
        
        for i in range(R):
            
            H[i,i,i]=sympy.core.symbol.Symbol(u+"{}".format(i+1))
            
        H[2,2,0]=H[2,0,2]=H[0,2,2]=-H[0,0,0]-H[1,1,0]
        H[2,2,1]=H[1,2,2]=H[2,1,2]=-H[1,1,1]-H[1,0,0]
        H[1,1,2]=H[1,2,1]=H[2,1,1]=-H[2,2,2]-H[2,0,0]
        
        return H
    
    def Harm_u(self,w):
        
        u =  MutableSparseNDimArray(range(3),(R,1))
        
        for i in range(R):
            u[i,0]=sympy.core.symbol.Symbol(w+"{}".format(i+1))
        
        return u
    
    
    def Harm_v(self,w):
        
        v =  MutableSparseNDimArray(range(3),(R,1))
        
        for i in range(R):
            v[i,0]=sympy.core.symbol.Symbol(w+"{}".format(i+1))
        
        return v
    
        
    

       

# class Symmetry(Tensor,Harm):
    
#     def __init__(self):
#         super().__init__(3,3)
        
    


class Transform(Tensor) :   
            

        
####################################################################################       
############ Methodes qui prennent P et retournent les harmoniques ################

    def Piez_To_Harm_a (self, P) :      
        
        
        
        l2=[]
        for i in range(R):  
            
            for j in range(R) :
                z1=0
                for p in range(R):
                    
                    for q in range(R):
                        
                        z1=factor(z1+(Rational(1,2)*(LeviCivita(i,p,q)*P[p,q,j]+(LeviCivita(j,p,q)*P[p,q,i]))))                
                l2.append(z1)
        a = MutableSparseNDimArray(l2, (R,R))  
        
        # print('\n[a] =', a)
        return a      
    
    
    def Piez_To_Harm_u (self, P, Decomp) :
            
        
        if Decomp=='CG' or Decomp=='Clebsch-Gordan'  :
            
            l3=[]
            for i in range(R): 
                z2=0
                for p in range(R):
                    
                    z2=simplify(factor(z2+P[p,p,i]-(Rational(1,3))*P[i,p,p]))
                l3.append(z2)                
            u = Array(l3, (R,1))  
                
            # print('\n[u] =', u)
            return u
        
        elif Decomp=='SW' or Decomp=='Schur-Weyl' : 
            
            l1=[]
            for i in range(R): 
                z1=0
                for p in range(R):
                   
                    z1=simplify(z1+Rational(1,3)*(P[i,p,p]+2*P[p,p,i]))
                
                l1.append(z1)
                
            u = Array(l1, (R,1))  
            
            # print('\n[u] =', u)
            return u
        
        else :
            print('\nInvalid input, please type Harm_u(P,"CG") or Harm_u(P,"Clebsch-Gordan") for Clebsh-Gordan decomposition or Harm_u(P,"SW") or Harm_u(P,"Schur-Weyl") for Schur-Weyl decomposition')
            sys.exit()
        
        
    def Piez_To_Harm_v (self, P, Decomp) :
        
        if Decomp=='CG' or Decomp=='Clebsch-Gordan'  :
            l4=[]
            for i in range(R): 
                z3=0
                for p in range(R):
                   
                    z3=z3+P[i,p,p]
                
                l4.append(z3)
                
            v = Array(l4, (R,1))
            # print('\n[v] =', v)
            return v
        
        elif Decomp=='SW' or Decomp=='Schur-Weyl' : 
            l2=[]
            for i in range(R): 
                z2=0
                for p in range(R):
                    
                    z2=z2+Rational(1,3)*(P[p,p,i]-P[i,p,p])
                
                l2.append(z2)
                
            v = Array(l2, (R,1))  
            # print('\n[v] =', v)
            return v
        
        else :
            print('\nInvalid input, please type Harm_v(P,"CG") or Harm_v(P,"SW") for Clebsh-Gordan decomposition or Harm_v(P,"SW") or Harm_v(P,"Schur-Weyl") for Schur-Weyl decomposition')
            sys.exit()
        
        
    def Piez_To_Harm_H (self, P, a, u, v, Decomp) :
        
        if Decomp=='CG' or Decomp=='Clebsch-Gordan'  :
            l5=[]
            l6=[]
            
            for i in range(R):  
                 
                for j in range(R) :
                    
                    for k in range(R):
                        z5=0
                        z4=0
                        z6=0
                        z7=0
                        for p in range(R):        
                            
                            z4=(z4-Rational(1,3)*(LeviCivita(i,j,p)*a[p,k]+LeviCivita(i,k,p)*a[p,j]))
                            z6=(-Rational(3,10)*(u[j,0]*KroneckerDelta(i,k)+u[k,0]*KroneckerDelta(i,j)-Rational(2,3)*u[i,0]*KroneckerDelta(j,k)))
                            z7=(-Rational(1,3)*v[i,0]*KroneckerDelta(j,k)   )
                            
                            z5=factor(P[i,j,k]+z4+z6+z7)
                              
                        l5.append(z5)
                        l6.append(z4)
                                                       
            H = Array(l5, (R,R,R))   #affichage matrice a
            # print('\n[H] =', H)
            return H
        
        elif Decomp=='SW' or Decomp=='Schur-Weyl' : 
            l4=[]
            z4=0
            for i in range(R):  
                 
                for j in range(R) :
                    
                    for k in range(R):
                        
                        z4=Rational(1,3)*(P[i,j,k]+P[k,i,j]+P[j,i,k])-Rational(1,5)*(KroneckerDelta(i,j)*u[k,0]+KroneckerDelta(i,k)*u[j,0]+KroneckerDelta(j,k)*u[i,0])
                        l4.append(z4)
            
            
            H = Array(l4,(R,R,R))
            # print('\n[H] =', H)
            return H
        
        else :
            print('\nInvalid input, please type Harm_H(P,"CG") or Harm_H(P,"Clebsch-Gordan") for Clebsh-Gordan decomposition or Harm_H(P,"SW") or Harm_H(P,"Schur-Weyl") for Schur-Weyl decomposition')
            sys.exit()

    

    def Harm_To_P12(self,H,a,u) :
        
        
        l7=[]
        
        for i in range(R):  
             
            for j in range(R) :
                
                for k in range(R):
                    z5=0
                    z4=0
                    z6=0
                    z7=0
                    for p in range(R):        
                        
                        # r=a.append(((0.5)*(LC[i,p,q]*P[p,q,j]+(LC[j,p,q]*P[p,q,i])))) 
                        z4=z4+Rational(1,3)*(LeviCivita(i,j,p)*a[p,k]+LeviCivita(i,k,p)*a[p,j])
                        z6=Rational(3,10)*(u[j,0]*KroneckerDelta(i,k)+u[k,0]*KroneckerDelta(i,j)-Rational(2,3)*u[i,0]*KroneckerDelta(j,k))
                         
                        
                        z8=factor(H[i,j,k]+z4+z6)
            
                    l7.append(z8)
        
        P12 = Array(l7, (R,R,R)) 
        # print('\n[P12] =', P12)
        return P12
    
    
    def Harm_To_P10(self,v) :
        
       
        l8=[]
        
        for i in range(R):  
             
            for j in range(R) :
                
                for k in range(R):
                    z5=0
                    z4=0
                    z6=0
                    z7=0
                    for p in range(R):        
                        
                        # r=a.append(((0.5)*(LC[i,p,q]*P[p,q,j]+(LC[j,p,q]*P[p,q,i])))) 
                        z7=Rational(1,3)*v[i,0]*KroneckerDelta(j,k)                                         
                        z9=factor(z7)  
                   
                    l8.append(z9)
        
        P10 = Array(l8, (R,R,R))   
        # print('\n[P10] =', P10)
        return P10
   
    
    def Harm_To_Ps(self,H,u) :
        
        
        l5=[]
        z5=0
        for i in range(R):  
             
            for j in range(R) :
                
                for k in range(R):
                    
                    z5=H[i,j,k]+Rational(1,5)*(KroneckerDelta(i,j)*u[k,0]+KroneckerDelta(i,k)*u[j,0]+KroneckerDelta(j,k)*u[i,0])
                    l5.append(z5)
        
        Ps = Array(l5,(R,R,R))
        # print('\n[Ps] =', Ps)
        return Ps
    
    
    def Harm_To_Pr(self,a,v) :
         

        l8=[]
        for i in range(R):  
             
            for j in range(R) :
                
                for k in range(R):
        
                    z6=0
                    for p in range(R):        
                        
                        # r=a.append(((0.5)*(LC[i,p,q]*P[p,q,j]+(LC[j,p,q]*P[p,q,i])))) 
                        z6=z6+Rational(1,3)*(LeviCivita(i,j,p)*a[p,k]+LeviCivita(i,k,p)*a[p,j])
                        z7=-Rational(1,2)*(2*v[i,0]*KroneckerDelta(j,k)-v[k,0]*KroneckerDelta(i,j)-v[j,0]*KroneckerDelta(i,k))
                        
                        z8=z6+z7
                        
                    #print(z4)   
                    #print(z5)   
                    
                    l8.append(z8)
        

        Pr = Array(l8,(R,R,R))
    
        # print('\n[Pr] =', Pr)
        return Pr


    

        
        
    def Harm_To_Piez(self,H,a,u,v,Decomp): 


        
        

        if Decomp=='CG' or Decomp=='Clebsch-Gordan'  :
        
            n = MutableSparseNDimArray(range(50),(R,R,R))  #initialisations
            r = MutableSparseNDimArray(range(50),(R,R,R))
            w = MutableSparseNDimArray(range(50),(R,R,R))     
            # n=simplify(P12+P10)
            l8=[]
            for i in range (R):    
                for j in range (R):
                    for k in range (R):
                        z1=0
                        for q in range(R):
                            z1 = z1+LeviCivita(i,j,q)*a[q,k]+LeviCivita(i,k,q)*a[q,j]
                        r[i,j,k] = H[i,j,k]+Rational(1,3)*z1+Rational(3,10)*((u[j,0])*KroneckerDelta(i,k)+u[k,0]*KroneckerDelta(i,j)-Rational(2,3)*u[i,0]*KroneckerDelta(j,k))+Rational(1,3)*v[i,0]*KroneckerDelta(j,k)
                        s=simplify(r)     
                        # w[i,j,k]=s[i,j,k]-P[i,j,k]                   
            return s
        
        
        elif Decomp=='SW' or Decomp=='Schur-Weyl' : 
            o = MutableSparseNDimArray(range(50),(R,R,R))  #initialisations
            l = MutableSparseNDimArray(range(50),(R,R,R))
            t = MutableSparseNDimArray(range(50),(R,R,R))
            
            o=simplify(Ps+Pr)
            
            
            
            l8=[]
            for i in range (R):    
                for j in range (R):
                    for k in range (R):
                        z1=0
                        for q in range(R):
                            z1 = z1+LeviCivita(i,j,q)*a[q,k]+LeviCivita(i,k,q)*a[q,j]
                        l[i,j,k] = H[i,j,k]+Rational(1,3)*z1+Rational(1,5)*((u[j,0])*KroneckerDelta(i,k)+u[k,0]*KroneckerDelta(i,j)+u[i,0]*KroneckerDelta(j,k))-Rational(1,2)*(2*v[i,0]*KroneckerDelta(j,k)-v[k,0]*KroneckerDelta(i,j)-v[j,0]*KroneckerDelta(i,k))
                        c=simplify(l)     
                        
                        
                        # t[i,j,k]=c[i,j,k]-P[i,j,k]   
    
            return c
            
        else :
            print('\nInvalid input, please type Harm_To_Piez(H,a,u,v,"CG") or Harm_To_Piez(H,a,u,v,"Clebsch-Gordan") for Clebsh-Gordan decomposition or Harm_To_Piez(H,a,u,v,"SW") or Harm_To_Piez(H,a,u,v,"Schur-Weyl") for Schur-Weyl decomposition')
            sys.exit()





    

    
    
    def Tr3(self,H): #Trace d'un tenseur ordre 3 (H[k,k,i]) placé dans un vecteur
        
        a=[]
        for i in range (R):
            ss=0
            for j in range(R):
                ss=simplify(H[j,j,i])+ss
            a.append(ss)
            v=MutableSparseNDimArray(a,(3,1))
        return v
    
    

###################################################################################


r = Tensor()

m = Tensor()

P1 = m.Tens('w',1,4)    #Tenseur ordre 1 dim 4 lettre w
P2 = m.Tens('x',2,8)    #Tenseur ordre 2 dim 8 lettre x
P3 = m.Tens('y',3,2)    #Tenseur ordre 3 dim 2 lettre y
P4 = m.Tens('z',4,3)    #Tenseur ordre 4 dim 3 lettre z
P5 = m.Tens('t',5,2)    #Tenseur ordre 5 dim 2 lettre t
# P6 = m.Tens('u',6,2)    #Tenseur ordre 6 dim 
P7 = m.Mat('Z',3,8)     #Matrice Lettre Z 3 ligne 8 colonnes

T3 = m.Tens3_Sym('S')      #Tenseur ordre 3 Totalement symetrique
Tp = m.Piezo_Symb('P')   #Tenseur piezo



q = m.Piezo_Num(1,2,3,5,6,9,10,11,12,14,15,18,19,20,21,23,24,27)


P = r.Piezo_Symb('P')            #Tenseur Piezo



p1 = r.Tens3_To_Kelv(P)       #Mat kelvin tiré de tenseur ordre 3 quelconques P
p2 = r.Tens3_To_Voigt(P)         #Mat voigt tiré de tenseur ordre 3 quelconques P

####################################################################################
######################## Decomp intrinsque sous Clebsh Gordan ######################

d = Harm()

a1 = d.Harm_a('a')
H1 = d.Harm_H('H')
u1 = d.Harm_u('u')
v1 = d.Harm_v('v')






####################################################################################
    

################ Harmoniques avec la décomposition de Clebsh-Gordan ################

d1 = Transform()  
     
a = d1.Piez_To_Harm_a(P)    
u = d1.Piez_To_Harm_u(P,'CG')
v = d1.Piez_To_Harm_v(P,'CG')
H = d1.Piez_To_Harm_H(P,a,u,v,"CG")      


ll = d1.Harm_To_Piez(H1,a1,u1,v1,"CG")   #construction de P piezo avec les harmonique intrinseques
                     #changer notation "reconstruction"


TrCG=d1.Tr3(H)      #Trace de l'harmonique H sous Clebsh-Gordan placé dans un vecteur


KelvH = d1.Tens3_To_Kelv(H)   #test Tens3_To_Kelv d'un tenseur 3 qqoncque 
VoigH = d1.Tens3_To_Voigt(H)

lm = d1.Tens3_To_Kelv(ll)    #mat kelv de ll ou on pourra travailler sur les symmetries plus tard

P10 = d1.Harm_To_P10(v)
P12 = d1.Harm_To_P12(H,a,u)





k=d1.Tens3_To_Kelv(d1.Harm_To_Piez(H1,a1,u1,v1,"CG"))

######### consrtuction de P a partir des harmoniques de Clebsh-Gordan #########


# n = MutableSparseNDimArray(range(50),(R,R,R))  #initialisations
# r = MutableSparseNDimArray(range(50),(R,R,R))
# w = MutableSparseNDimArray(range(50),(R,R,R))

# n=simplify(P12+P10)



# l8=[]
# for i in range (R):    
#     for j in range (R):
#         for k in range (R):
#             z1=0
#             for q in range(R):
#                 z1 = z1+LeviCivita(i,j,q)*a[q,k]+LeviCivita(i,k,q)*a[q,j]
#             r[i,j,k] = H[i,j,k]+Rational(1,3)*z1+Rational(3,10)*((u[j,0])*KroneckerDelta(i,k)+u[k,0]*KroneckerDelta(i,j)-Rational(2,3)*u[i,0]*KroneckerDelta(j,k))+Rational(1,3)*v[i,0]*KroneckerDelta(j,k)
#             s=simplify(r)     #s = tenseur 3 P normallement
            
            
#             w[i,j,k]=s[i,j,k]-P[i,j,k]   #tenseur nulle normallement
            

# print('\n[n] =', n)     # P12+P10=P                               vérifié
# print('\n[s] =', s)     # Decomp harmonique forme le tenseur P    vérifié


#ectriture avec Rational(a,b) permet d'eviter le 0 numérique

#Resultat : on retrouve bien le tenseur P donc la decomposition a été construite correctement
# dans le cas de Clebsch-Gordan

####################################################################################


############## Harmoniques avec la décomposition de Schur-Weyl ##############

  

aa = d1.Piez_To_Harm_a(P)
uu = d1.Piez_To_Harm_u(P,"SW")
vv = d1.Piez_To_Harm_v(P,"SW")
HH = d1.Piez_To_Harm_H(P,aa,uu,vv,"SW")  

TrSW=d1.Tr3(HH)      #Trace de l'harmonique H sous Clebsh-Gordan placé dans un vecteur


Ps = d1.Harm_To_Ps(HH,uu)
Pr = d1.Harm_To_Pr(aa,vv)


########## consrtuction de P a partir des harmoniques de Schur-Weyl  ##########


# o = MutableSparseNDimArray(range(50),(R,R,R))  #initialisations
# l = MutableSparseNDimArray(range(50),(R,R,R))
# t = MutableSparseNDimArray(range(50),(R,R,R))

# o=simplify(Ps+Pr)



# l8=[]
# for i in range (R):    
#     for j in range (R):
#         for k in range (R):
#             z1=0
#             for q in range(R):
#                 z1 = z1+LeviCivita(i,j,q)*a[q,k]+LeviCivita(i,k,q)*a[q,j]
#             l[i,j,k] = HH[i,j,k]+Rational(1,3)*z1+Rational(1,5)*((uu[j,0])*KroneckerDelta(i,k)+uu[k,0]*KroneckerDelta(i,j)+uu[i,0]*KroneckerDelta(j,k))-Rational(1,2)*(2*vv[i,0]*KroneckerDelta(j,k)-vv[k,0]*KroneckerDelta(i,j)-vv[j,0]*KroneckerDelta(i,k))
#             c=simplify(l)     #c = tenseur 3 P normallement
            
            
#             t[i,j,k]=c[i,j,k]-P[i,j,k]   #tenseur nulle normallement si decomp correcte
            
            
# print('\n[o] =', np.array(o))     # Ps+Pr=P                                   vérifié
# print('\n[c] =', np.array(c))     # Decomp harmonique forme le tenseur P      vérifié



#Resultat : on retrouve bien le tenseur P donc la decomposition a été construite correctement
# dans le cas de Schur-Weyl



# Sur console, taper  simplify(P10+P12)   ou    s pour decomp CG
# Sur console, taper  simplify(Ps+Pr)     ou    c pour decomp SW

# s=0
# for i in range(R):
#         for j in range(R):
#             for k in range(R):
#                 s=s+a[k,k]
#             if (a[i,j]==a[j,i]) and s==0:
#                 a[1,2]=a[2,1]=sympy.core.symbol.Symbol("a{}".format(3))
#                 a[0,2]=a[2,0]=sympy.core.symbol.Symbol("a{}".format(4))
#                 a[0,1]=a[1,0]=sympy.core.symbol.Symbol("a{}".format(5))
#                 for l in range(R):
#                     a[l,l]=sympy.core.symbol.Symbol("a{}".format(l+1))
#                     a[2,2]=-a[0,0]-a[1,1]
                
    
#Si s==0 donc trace nulle et a[i,j]=a[j,i] donc symétrique, on injecte les 5 composantes de a
        


# Pour verifier nombres de composantes independantes ecrire sur console :
    
################################### H #########################################

# H[1,1,0]==H[1,0,1]==H[0,1,1]      H4 
# H[1,0,0]==H[0,1,0]==H[0,0,1]      H5          
# H[2,0,0]==H[0,2,0]==H[0,0,2]      H6
# H[2,2,0]==H[2,0,2]==H[0,2,2]      -H1-H4      
# H[2,2,1]==H[1,2,2]==H[2,1,2]      -H2-H5      
# H[1,1,2]==H[1,2,1]==H[2,1,1]      -H3-H6      

# H[0,0,0]      H1
# H[1,1,1]      H2     
# H[2,2,2]      H3     
# H[0,1,2]==H[0,2,1]==H[2,0,1]==H[2,1,0]==H[1,2,0]==H[1,0,2]      H7   
# 29 composantes avec 10 independantes
# H[0,0,0]+H[0,1,1]+H[0,2,2]
# H[1,0,0]+H[1,1,1]+H[1,2,2]
# H[2,0,0]+H[2,1,1]+H[2,2,2]
# ==> 7 composantes independantes

###############################################################################
################################### a ########################################

# a[1,2]=a[2,1]
# a[0,2]=a[2,0]
# a[1,0]=a[0,1]
# a[2,2]=-a[1,1]-a[0,0]


eps = m.Tens(str(symbols('epsilon')),2,3)
ddd = m.Tens('d',1,3)


for i in range(R):
    s=0
    for j in range (R):
        for k in range (R):
            s=s+ll[i,j,k]*eps[j,k]
            # print('\ns',s)
        ddd[i,0]=s

        


