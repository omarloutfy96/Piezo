from Piezo_mirror import Tensor, Harm, Transform
from sympy import *

d = Tensor()
b = Harm()
c = Transform()

########################### Fonction Tensorielles #############################


T1 = d.Tens('a',1,6)    #Tenseur ordre 1 dim 4 lettre a
T2 = d.Tens('b',2,5)    #Tenseur ordre 2 dim 8 lettre b
T25 = d.Tens('gamma',2,[2,5])
T3 = d.Tens('c',3,4)    #Tenseur ordre 3 dim 2 lettre c
T4 = d.Tens('d',4,3)    #Tenseur ordre 4 dim 3 lettre d
T5 = d.Tens('e',5,2)    #Tenseur ordre 5 dim 2 lettre e
T6 = d.Tens3_Sym('f')   #Tenseur 3 totalement symetrique

T7 = d.Mat('g',4,2)     #Matrice 4 ligne 2 colonnes lettre g

P = d.Piezo_Symb('P')   #Tenseur piezo symbolique

P1 = d.Piezo_Num(1,2,3,5,6,9,10,11,12,14,15,18,19,20,21,23,24,27)


K = d.Tens3_To_Kelv(P)
K1 = d.Tens3_To_Kelv(P1)

V = d.Tens3_To_Voigt(P)
V1 = d.Tens3_To_Voigt(P1)


###############################################################################
######################## Harmoniques intrinsèques #############################


a = b.Harm_a('a')
H = b.Harm_H('H')
u = b.Harm_u('u')



CG0 = c.Harm_To_Piez(H,a,u,u,'CG')    #reconstruction Clebsch-Gordan avec harmoniques intrinsèques
SW0 = c.Harm_To_Piez(H,a,u,u,"SW")    #reconstruction Schur-Weyl avec harmoniques intrinsèques


###############################################################################
################################Clebcsh-Gordan#################################

# 1) Harmonique d'un tenseur symbolique

a11 = c.Piez_To_Harm_a(P)
u11 = c.Piez_To_Harm_u(P,"CG")
v11 = c.Piez_To_Harm_v(P,"CG")
H11 = c.Piez_To_Harm_H(P,a11,u11,v11,"CG")


# 2) Harmonique d'un tenseur numérique

a12 = c.Piez_To_Harm_a(P1)
u12 = c.Piez_To_Harm_u(P1,"CG")
v12 = c.Piez_To_Harm_v(P1,"CG")
H12 = c.Piez_To_Harm_H(P1,a12,u12,v12,"CG")


# 3) Reconstruction

CG1 = c.Harm_To_Piez(H11, a11, u11, v11, "CG")    #reconstruction de P symbolique

CG2 = c.Harm_To_Piez(H12, a12, u12, v12, "CG")    #reconstruction de P numérique


R1=simplify(c.Harm_To_P12(H11,a11,u11)+c.Harm_To_P10(v11))   #P10+P12

##############################################################################
#################################Schur-Weyl###################################

# 1) Harmonique d'un tenseur symbolique

a21 = c.Piez_To_Harm_a(P)
u21 = c.Piez_To_Harm_u(P,"SW")
v21 = c.Piez_To_Harm_v(P,"SW")
H21 = c.Piez_To_Harm_H(P,a21,u21,v21,"SW")


# 2) Harmonique d'un tenseur numérique

a22 = c.Piez_To_Harm_a(P1)
u22 = c.Piez_To_Harm_u(P1,"SW")
v22 = c.Piez_To_Harm_v(P1,"SW")
H22 = c.Piez_To_Harm_H(P1,a22,u22,v22,"SW")


# 3) Reconstruction

SW1 = c.Harm_To_Piez(H21, a21, u21, v21, "SW")    #reconstruction de P symbolique

SW2 = c.Harm_To_Piez(H22, a22, u22, v22, "SW")    #reconstruction de P numérique


R2=simplify(c.Harm_To_Ps(H21,u21)+c.Harm_To_Pr(a21,v21))   #Ps+Pr

##############################################################################
########################## Tests quelconques #################################

KelvP = d.Tens3_To_Kelv(P)
KelvP1 = d.Tens3_To_Kelv(P1)

KelvCG0 = d.Tens3_To_Kelv(CG0)
VoigtCG0 = d.Tens3_To_Voigt(CG0)






cx = [1,0,0,0,cos('alpha'),-sin('alpha'),0,sin('alpha'),cos('alpha')]
cy = [cos('omega'),0,sin('omega'),0,1,0,-sin('omega'),0,cos('omega')]
cz = [cos('theta'),-sin('theta'),0,sin('theta'),cos('theta'),0,0,0,1]
# cz = [cos(pi/4),-sin(pi/4),0,sin(pi/4),cos(pi/4),0,0,0,1]


Rx = MutableSparseNDimArray(cx,(3,3))
Ry = MutableSparseNDimArray(cy,(3,3))
Rz = MutableSparseNDimArray(cz,(3,3))


R = Matrix(Rz) #*Matrix(Ry)  #*Matrix(R3)
P2 = MutableDenseNDimArray.zeros(3,3,3)

# for i in range(3):
#     for j in range(3):
#         for k in range(3):
            
            
            
#             P2[i,j,k] = R[i,1]*R[j,1]*R[k,1]*P1[1,1,1]+R[i,1]*R[j,2]*R[k,1]*P1[1,2,1]+R[i,2]*R[j,1]*R[k,1]*P1[2,1,1]+R[i,2]*R[j,2]*R[k,1]*P1[2,2,1]+R[i,1]*R[j,1]*R[k,2]*P1[1,1,2]+R[i,1]*R[j,2]*R[k,2]*P1[1,2,2]+R[i,2]*R[j,1]*R[k,2]*P1[2,1,2]+R[i,2]*R[j,2]*R[k,2]*P1[2,2,2]
            
            # for p in range(3):
                
            #     for q in range(3):
            #         s=0
            #         for r in range(3):
            #             s = s + R[i,p]*R[j,q]*R[k,r]*P1[p,q,r]
            #             print('\ns=',s)
            

for i in range(3):
    for j in range(3):
        for k in range(3):
            
            s=0
            for p in range(3):
                
                for q in range(3):
                    
                    for r in range(3):
                        s = s + R[i,p]*R[j,q]*R[k,r]*P1[p,q,r]
                        
                        
            P2[i,j,k]=simplify(s)







############# A REGLER ROTATION 3D NON COMMUTATIVE SI  2 OU PLUS ROTATION D'AXE DIFFERERENTS ############
########################### FAIT POUR DIM = 3 A REGLER POUR DIM 2 #############################




P2 = Transform().Rot_Tens(P1,[pi,pi/2,pi/4])




u1111 = Transform().Piez_To_Harm_u(P2[0],'CG')  #decomp CG apres rotation 
u1112 = Matrix(P2[1])*Matrix(u12)


v1111 = Transform().Piez_To_Harm_v(P2[0],'CG')  #decomp CG apres rotation 
v1112 = Matrix(P2[1])*Matrix(v12)




############### TEST DE ROTATION DES TENSEUR !!! ########################
########################### ORDRE 3 ##############################

P5 = Tensor().Piezo_Num(7,9,5,74,25,8,35,4,2,23,4,5,1,18,43,6,87,17) #Tenseur original
P5T = Transform().Rot_Tens(P5,pi/6) ### Tenseur apres rotation 

zz = Transform().Piez_To_Harm_u(P5T[0],'CG') 
zzz = Matrix(P5T[1])*Matrix(Transform().Piez_To_Harm_u(P5,'CG'))


a1111 = simplify (Matrix(Transform().Piez_To_Harm_a(P5T[0])))
a1112 = simplify (P5T[1]*Matrix(Transform().Piez_To_Harm_a(P5))*transpose(P5T[1]))


P6T = Transform().Rot_Tens(P5,[pi/2,pi/4,pi/8])
yy = Transform().Piez_To_Harm_u(P6T[0],'CG')
yyy = simplify(Matrix(P6T[1])*Matrix(Transform().Piez_To_Harm_u(P5,'CG')))

P7T = Transform().Rot_Tens(P5,[pi/2,pi/4,pi/8])
yys = Transform().Piez_To_Harm_u(P7T[0],'SW')
yyys = simplify(Matrix(P7T[1])*Matrix(Transform().Piez_To_Harm_u(P5,'SW')))


###########################  ORDRE 2  ##############################


rr = Transform().Rot_Tens(a,pi/6)
rrr = simplify(Matrix(rr[1]).inv()*Matrix(rr[0])*transpose(Matrix(rr[1]).inv())) 


##################### g*P=P si rotation π % 3 axes ######################

# Tensor().Rot_Tens(P,[pi,pi,pi])[0]==P verifier que c'est vrai

Prot = Tensor().Rot_Tens(P,[pi/2,pi/4,pi/8])

s=0
for i in range(3):
    for k in range(3):
        for p in range(3):
            s=s+Prot[0][i,i,k]*Prot[0][p,p,k]
inv1 = simplify(s)

l=0
for i in range(3):
    for k in range(3):
        for p in range(3):
            l=l+P[i,i,k]*P[p,p,k]

inv11 = simplify(l)




